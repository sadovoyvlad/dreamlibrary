//
//  CategoriesPresenter.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 27.09.2023.
//

import Foundation
import UIKit

protocol CategoriesPresenterOutPut: AnyObject {
    func updateTableView()
}

class CategoriesPresenter {
    weak var output: CategoriesPresenterOutPut?
    var categories: CategoriesModel?
}

extension CategoriesPresenter: CategoriesViewControllerOutPut {
    func getCategories() {
        ApiManager.shared.getCategories(completion: { [weak self] categorisModel in
            self?.categories = categorisModel
            self?.output?.updateTableView()
        })
    }
}
