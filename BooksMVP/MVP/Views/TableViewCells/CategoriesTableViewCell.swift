//
//  CategoriesTableViewCell.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 28.09.2023.
//

import Foundation
import UIKit
import SnapKit

class CategoriesTableViewCell: UITableViewCell {

    var bookTitleLabel = UILabel()
    var bookPublishDateLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(bookTitleLabel)
        addSubview(bookPublishDateLabel)

        backgroundColor = UIColor.gray

        configureBookTitleLabel()
        configureBookPublishDateLabel()
        setBookTitleLabelConstraints()
        setBookPublishLabelConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func set(book: Result) {
        bookTitleLabel.text = book.listName
        bookTitleLabel.textColor = .black
        bookPublishDateLabel.text = book.newestPublishedDate
    }

    func configureBookTitleLabel() {
        bookTitleLabel.numberOfLines = 0
        bookTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
    }

    func configureBookPublishDateLabel() {
        bookPublishDateLabel.numberOfLines = 0
        bookPublishDateLabel.adjustsFontSizeToFitWidth = true
    }

    func setBookTitleLabelConstraints() {
        bookTitleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(-10)
            make.left.equalToSuperview().offset(20)
        }
    }

    func setBookPublishLabelConstraints() {
        bookPublishDateLabel.snp.makeConstraints { make in
            make.top.equalTo(bookTitleLabel.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(20)
        }
    }
}
