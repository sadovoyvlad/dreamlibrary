////
////  BooksViewController.swift
////  BooksMVP
////
////  Created by Vlad Vladyslav on 27.09.2023.
////
//
//import Foundation
//
//import UIKit
//
//class TopChartVC: UIViewController {
//
//     // MARK: Variables
//    private var tableView = UITableView()
//    private var data: Categories?
//
//    private struct Cells {
//        static let topChartCell = "TopChartCell"
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        navigationController?.navigationBar.prefersLargeTitles = true
//        title = "Categories"
//
//        backgroundColorSetUp()
//        configureTableView()
//        makeRequest()
//    }
//
//    // MARK: - Configure_TableView
//    func configureTableView() {
//        view.addSubview(tableView)
//        setTableViewDelegates()
//        tableView.rowHeight = 100
//        tableView.backgroundColor = UIColor(named: "middleGrey")
//        tableView.register(TopChartCell.self, forCellReuseIdentifier: Cells.topChartCell)
//        setUpConstraints()
//
//    }
//
//    // MARK: - Set TableView Delegates
//    func setTableViewDelegates() {
//        tableView.delegate = self
//        tableView.dataSource = self
//    }
//
//    // MARK: - Set up background color
//    func backgroundColorSetUp() {
//        view.backgroundColor = UIColor(named: "middleGrey")
//    }
//
//    // MARK: - Set up constraints
//    func setUpConstraints() {
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
//            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
//            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
//            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 150)
//        ])
//    }
//
//    // MARK: - Make URL Request
//    private func makeRequest() {
//        let request = URLRequest(url: URL(string: "https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=FmOYuGGerZeuJWWbuwkQ6vlKkhbd4xk6")!)
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            if let data = data, let categories = try? JSONDecoder().decode(Categories.self, from: data) {
//                print(categories.results?.first?.listName)
//                self.data = categories
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//            }
//        }
//        task.resume()
//    }
//}
//
//// MARK: - Set TableView Delegate And DataSource
//extension TopChartVC: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        data?.results?.count ?? .zero
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        //swiftlint:disable force_cast
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.topChartCell) as? TopChartCell else {
//            fatalError("could not cast cell")
//        }
////        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.topChartCell) as! TopChartCell
//        if let book = data?.results?[indexPath.row] {
//            cell.set(book: book)
//        }
//
//        cell.selectionStyle = .none
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let selectedItem = data?.results?[indexPath.row].listNameEncoded
//        let nextVC = TopBookDetailsVC()
//        nextVC.categoryName = selectedItem
//
//        self.navigationController?.pushViewController(nextVC, animated: true)
//
//    }
//}
