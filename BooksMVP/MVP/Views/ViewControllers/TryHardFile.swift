//
//  TryHardFile.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 08.10.2023.
//

import Foundation
import UIKit

protocol CategoriesViewControllerOutPut {
    var categories: CategoriesModel? { get }
    func getCategories()
}

protocol SendDataToEnumProtocol {
    func sendDataToEnum(data: String)
}

//protocol CategoriesViewControllerDelegate: class {
//    func didUpdateSelectedCategory(currentCatagory: String)
//}

class TryHardVC: UIViewController {
    
    private var tableView = UITableView()
    
    private struct Cells {
        static let categoryCell = "CategoriesTableViewCell"
    }
//    weak var categoriesDelegate: CategoriesViewControllerDelegate?
//    weak var dataDelegate: SendDataToEnumProtocol?
    
    private lazy var output: CategoriesViewControllerOutPut = {
        let presenter = CategoriesPresenter()
        presenter.output = self
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.getCategories()
        configureNavigationController()
        backgroundColorSetUp()
        configureTableView()
    }
    
    //MARK: - Configure_NavigationController
    private func configureNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Categories"
    }
    
    //MARK: - Configure_TableView
    func configureTableView() {
        view.addSubview(tableView)
        setTableViewDelegates()
        tableView.rowHeight = 100
        tableView.backgroundColor = UIColor.lightGray
        tableView.register(CategoriesTableViewCell.self, forCellReuseIdentifier: Cells.categoryCell)
        setUpConstreints()
        
        
    }
    
    //MARK: - Set_TableViewDelegates
    func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK: - Set_Background_Color
    func backgroundColorSetUp() {
        view.backgroundColor = UIColor.darkGray
    }
    
    //MARK: - Set_Constraints
    func setUpConstreints() {
        tableView.snp.makeConstraints { make in
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.top.equalTo(self.view).offset(150)
        }
    }
}


//MARK: - TableViewDelegate, TableViewDataSource
extension TryHardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        output.categories?.results.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.categoryCell) as? CategoriesTableViewCell else {
            fatalError("could not cast cell")
        }
        guard let category = output.categories?.results else {
            return UITableViewCell()
        }
        cell.set(book: category[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCategory = output.categories?.results[indexPath.row].listNameEncoded
        
//        if dataDelegate != nil {
//            self.dataDelegate?.sendDataToEnum(data: currentCategory!)
//        }
//        categoriesDelegate?.didUpdateSelectedCategory(currentCatagory: currentCategory!)
    }
    
}

extension TryHardVC: CategoriesPresenterOutPut {
    func updateTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
