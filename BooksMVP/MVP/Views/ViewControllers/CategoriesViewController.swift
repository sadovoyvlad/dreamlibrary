//
//  CategoriesViewController.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 27.09.2023.
//

//import Foundation
//import UIKit
//import SnapKit
//
//class CategoriesViewController: UIViewController, CategoriesPresenterDelegate {
//
//    //MARK: Variables
//    private var categories: CategoriesModel?
//    private var tableView = UITableView()
//    private var data: DescriptionModel?
//
//    var a: CategoriesModel?
//
//
//    private struct Cells {
//        static let categoryCell = "CategoriesTableViewCell"
//    }
//
//    private let presenter = CategoriesPresenter()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        ApiManager.shared.getCategories { CategoriesList in
//            self.a = CategoriesList
//        }
//
//        configureNavigationController()
//        backgroundColorSetUp()
//        configureTableView()
//
//        presenter.setViewDelegate(delegate: self)
////        presenter.getCategories()
//
//    }
//
//    //MARK: - Configure_NavigationController
//    private func configureNavigationController() {
//        navigationController?.navigationBar.prefersLargeTitles = true
//        title = "Categories"
//    }
//
//    //MARK: - Configure_TableView
//    func configureTableView() {
//        view.addSubview(tableView)
//        setTableViewDelegates()
//        tableView.rowHeight = 100
//        tableView.backgroundColor = UIColor.lightGray
//        tableView.register(CategoriesTableViewCell.self, forCellReuseIdentifier: Cells.categoryCell)
//        setUpConstreints()
//
//
//    }
//
//    //MARK: - Set_TableViewDelegates
//    func setTableViewDelegates() {
//        tableView.delegate = self
//        tableView.dataSource = self
//    }
//
//    //MARK: - Set_Background_Color
//    func backgroundColorSetUp() {
//        view.backgroundColor = UIColor.darkGray
//    }
//
//    //MARK: - Set_Constraints
//    func setUpConstreints() {
//        tableView.snp.makeConstraints { make in
//            make.left.equalTo(self.view)
//            make.right.equalTo(self.view)
//            make.bottom.equalTo(self.view)
//            make.top.equalTo(self.view).offset(150)
//        }
//    }
//}
//
////MARK: - TableViewDelegate, TableViewDataSource
//extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        categories?.results.count ?? .zero
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.categoryCell) as? CategoriesTableViewCell else {
//            fatalError("could not cast cell")
//        }
//        guard let category = categories?.results else {
//            return UITableViewCell()
//        }
//        cell.set(book: category[indexPath.row])
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//    }
//
//    //MARK: - PresenterDelegate
//    func presentCategories(categories: CategoriesModel) {
//        self.a = categories
//
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//    }
//
//
//}
