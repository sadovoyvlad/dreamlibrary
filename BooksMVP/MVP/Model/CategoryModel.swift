//
//  DescriptionModel.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 27.09.2023.
//

import Foundation

// MARK: - Categories
struct CategoriesModel: Codable {
    let status, copyright: String?
    let numResults: Int?
    let results: [Result]

    enum CodingKeys: String, CodingKey {
        case status, copyright
        case numResults = "num_results"
        case results
    }
}

// MARK: - Result
struct Result: Codable {
    let listName, displayName, listNameEncoded, oldestPublishedDate: String?
    let newestPublishedDate: String?
    let updated: Updated?

    enum CodingKeys: String, CodingKey {
        case listName = "list_name"
        case displayName = "display_name"
        case listNameEncoded = "list_name_encoded"
        case oldestPublishedDate = "oldest_published_date"
        case newestPublishedDate = "newest_published_date"
        case updated
    }
}

enum Updated: String, Codable {
    case monthly = "MONTHLY"
    case weekly = "WEEKLY"
}

extension Result {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.listName = try values.decodeIfPresent(String.self, forKey: .listName)
        self.displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
        self.listNameEncoded = try values.decodeIfPresent(String.self, forKey: .listNameEncoded)
        self.oldestPublishedDate = try values.decodeIfPresent(String.self, forKey: .oldestPublishedDate)
        self.newestPublishedDate = try values.decodeIfPresent(String.self, forKey: .newestPublishedDate)
        self.updated = try values.decodeIfPresent(Updated.self, forKey: .updated)
    }
}
