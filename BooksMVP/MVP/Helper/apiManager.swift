//
//  apiManager.swift
//  BooksMVP
//
//  Created by Vlad Vladyslav on 07.10.2023.
//

import Foundation

enum ApiType {
    case getCategories
    case getBooks(data: String)
    var request: URLRequest {
        switch self {
        case .getBooks(let data):
            let urlString =  "https://api.nytimes.com/svc/books/v3/lists/current/\(data).json?api-key=FmOYuGGerZeuJWWbuwkQ6vlKkhbd4xk6"
            guard let url = URL(string: urlString) else {
                fatalError("ivalid url")
            }
            let request = URLRequest(url: url)
            return request
            
        case .getCategories:
            let url = URL(string: "https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=FmOYuGGerZeuJWWbuwkQ6vlKkhbd4xk6")!
            let request = URLRequest(url: url)
            return request
        }
    }
}

class ApiManager {
    
    static let shared = ApiManager()
    
    func getDiscription(comletion: @escaping (DescriptionModel) -> Void) {
        var request = ApiType.getBooks(data: <#T##String#>).request
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let descriprion = try JSONDecoder().decode(DescriptionModel.self, from: data)
                comletion(descriprion)
            }
            catch {
                print(error)
            }
        }
        task.resume()
    }
    
    func getCategories(completion: @escaping (CategoriesModel) -> Void) {
        let request = ApiType.getCategories.request
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let categories = try JSONDecoder().decode(CategoriesModel.self, from: data)
                completion(categories)
            }
            catch {
                print(error)
            }
        }
        task.resume()
    }
}


//  public func getCategories() {
//let request = URLRequest(url: URL(string: C.path)!)
//let task = URLSession.shared.dataTask(with: request) { [weak self] data, _, error in
//    guard let data = data, error == nil else {
//        return
//    }
//        do {
//            let categories = try JSONDecoder().decode(CategoriesModel.self, from: data)
//            self?.delegate?.presentCategories(categories: categories)
//        }
//        catch {
//            print(error)
//        }
//    }
//    task.resume()
//}
